# Fate Companion

## General Information
This app uses both, [Angular]() and [Firebase](). Since both frameworks offer 
generators, this project utilizes both. This leads (among others) to the fact 
that it can be served locally in two ways (through angular and firebase 
interal servers). Keep that in mind.

## Setup

### General
1.  Get [NPM](https://www.npmjs.com/get-npm) or update to the latest version 
(`npm install npm@latest -g`)
2. Get Yarn (`npm install -g yarn`)
3.  Install dependencies of the project (`yarn install`)
4.  Make sure, your system knows all `ng`-commands (`npm link @angular/cli`)

### Firebase changes and Deployment
1.  Setup general project as described above.
2.  Login to firebase (`firebase login`)
3.  Serve locally through firebase for testing (`firebase serve --only hosting`)
4.  Deploy via (`firebase deploy -m "release message"`)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app 
will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can 
also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the 
`dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via 
[Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via 
[Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the 
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).