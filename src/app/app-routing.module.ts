import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {GameComponent} from './game/game.component';
import {HomeComponent} from './home/home.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';

import {AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo} from '@angular/fire/auth-guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToHome = () => redirectLoggedInTo(['home']);


const routes: Routes = [
  {path: '', redirectTo: '/landing-page', pathMatch: 'full'},
  {path: 'game/:gameId', component: GameComponent},
  {
    path: 'home',
    component: HomeComponent,
    data: {authGuardPipe: redirectUnauthorizedToLogin},
    canActivate: [AngularFireAuthGuard]
  },
  {
    path: 'landing-page',
    component: LandingPageComponent,
    data: {authGuardPipe: redirectLoggedInToHome},
    canActivate: [AngularFireAuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {authGuardPipe: redirectLoggedInToHome},
    canActivate: [AngularFireAuthGuard]
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    data: {authGuardPipe: redirectLoggedInToHome},
    canActivate: [AngularFireAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
