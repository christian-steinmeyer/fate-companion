import {Component} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  title = 'fate-companion';

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'baby',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/baby.svg`),
    );
    this.matIconRegistry.addSvgIcon(
      'planet',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/earth.svg')
    );
  }
}
