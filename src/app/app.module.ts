import {environment} from '../environments/environment';

import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {DialogsModule} from './dialogs/dialogs.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatStepperModule} from '@angular/material/stepper';
import {MatToolbarModule} from '@angular/material/toolbar';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FooterComponent} from './footer/footer.component';
import {GameComponent} from './game/game.component';
import {HomeComponent} from './home/home.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {SpeedDialFabComponent} from './ui/speed-dial-fab/speed-dial-fab.component';
import {ToolbarComponent} from './toolbar/toolbar.component';

import {AuthenticationService} from './services/authentication/authentication.service';
import {ValidationService} from './services/validation/validation.service';
import {MatStepperFooterComponent} from './ui/mat-stepper-footer/mat-stepper-footer.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    GameComponent,
    HomeComponent,
    LandingPageComponent,
    LoginComponent,
    SignUpComponent,
    SpeedDialFabComponent,
    ToolbarComponent,
    MatStepperFooterComponent
  ],
  imports: [
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'fate-companion'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    CdkStepperModule,
    DialogsModule,
    FormsModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatToolbarModule,
    ReactiveFormsModule,
  ],
  providers: [AuthenticationService, AngularFireAuthGuard, ValidationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
