import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ConfirmDialogComponent, ConfirmDialogContext} from './confirmation/confirm-dialog.component';

@Injectable()
export class DialogService {
  constructor(private dialog: MatDialog) {
  }

  public openConfirmDialog(data: ConfirmDialogContext) {
    return this.dialog.open(ConfirmDialogComponent, {data}).afterClosed();
  }
}
