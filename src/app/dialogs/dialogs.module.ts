import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {DialogService} from './dialog.service';
import {MatButtonModule, MatDialogModule} from '@angular/material';
import {ConfirmDialogComponent} from './confirmation/confirm-dialog.component';


@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [
    ConfirmDialogComponent
  ],
  exports: [ConfirmDialogComponent],
  entryComponents: [ConfirmDialogComponent],
  providers: [DialogService]
})
export class DialogsModule {
}
