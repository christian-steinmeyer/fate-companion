import {Component, OnInit} from '@angular/core';

export interface Tile {
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  constructor() {
  }

  tiles: Tile[] = [
    {text: 'IMG', cols: 1, rows: 2},
    {text: 'privacy', cols: 1, rows: 1},
    {text: 'imprint', cols: 1, rows: 1},
    {text: 'GTC', cols: 1, rows: 1},
    {text: 'cookies', cols: 1, rows: 1}
  ];

  ngOnInit() {
  }

}
