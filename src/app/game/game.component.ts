import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';

import {ValidationService} from '../services/validation/validation.service';
import {MatAutocompleteSelectedEvent, MatVerticalStepper, ThemePalette} from '@angular/material';
import {AttributeDefinition, SkillCombination, SkillDefinition} from '../../lib/data.types/skill';
import {StressBarDefinition} from '../../lib/data.types/stress';
import {DialogService} from '../dialogs/dialog.service';
import {Aggregation, DefaultAggregations, DefaultRoundings, Rounding} from '../../lib/math/calculation';
import {delete_item_from_array} from '../../lib/utils/arrays';
import {DebugGame, DefaultGame, GameDefinition} from '../../lib/data.types/gameDefinition';
import {RuleSet, RuleSets} from '../../lib/data.types/rules';
import {FilterService} from '../services/filter.service';
import {Namable} from '../../lib/data.types/base';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit {
  private gameDoc: AngularFirestoreDocument<GameDefinition>;
  aggregations: Aggregation[] = DefaultAggregations;
  roundings: Rounding[] = DefaultRoundings;
  color: ThemePalette = 'accent';
  game: GameDefinition = DefaultGame;
  ruleSetOptions: RuleSet[] = RuleSets;
  separatorKeysCodes: number[] = [COMMA, ENTER];
  generalForm: FormGroup;
  skillForm: FormGroup;
  stressForm: FormGroup;
  stuntForm: FormGroup;
  extraForm: FormGroup;
  participantForm: FormGroup;

  constructor(
    private afs: AngularFirestore,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public validator: ValidationService,
    private dialogService: DialogService,
    public filters: FilterService
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const gameId = params.get('gameId');
      this.gameDoc = this.afs.doc<GameDefinition>('games/' + gameId);
    });

    this.generalForm = this.formBuilder.group({
      title: [this.game.name, Validators.required],
      description: [this.game.description],
      ruleSet: [this.game.ruleSet, Validators.required]
    });
    this.skillForm = this.formBuilder.group({
      useAttributes: [this.game.useAttributes],
      skill: [''],
      skills: [this.game.skills],
      attribute: [''],
      attributes: [this.game.attributes],
    }, {validator: this.validator.atLeastOne(Validators.required, ['skills', 'attributes'])});
    this.stressForm = this.formBuilder.group({
      name: [''],
      refreshRate: [''],
      stressBar: [''],
      stressBars: [this.game.stressBars],
    });
    this.stuntForm = this.formBuilder.group({
      numberOfFreeStunts: [this.game.numberOfFreeStunts, [Validators.required, Validators.min(0)]],
      hasMaxNumberOfStunts: [this.game.hasMaxNumberOfStunts],
      numberOfMaxStunts: [this.game.numberOfMaxStunts, Validators.min(this.game.numberOfFreeStunts)],
      costPerStunt: [this.game.costPerStunt, [Validators.required, Validators.min(0)]]
    }, {validator: this.validator.noneOrAll(Validators.required, ['hasMaxNumberOfStunts', 'numberOfMaxStunts'])});
    this.extraForm = this.formBuilder.group({
      useInventory: [this.game.useInventory],
      useMoney: [this.game.useMoney],
      useExtras: [this.game.useExtras]
    });
    this.participantForm = this.formBuilder.group({ });

    for (const attribute of this.game.attributes) {
      this._addFormControlsForSkillCombination(this.skillForm, attribute);
    }

    for (const stressBar of this.game.stressBars) {
      this._addFormControlsForSkillCombination(this.stressForm, stressBar);
    }
  }

  private processMatChipInputEvent(event: MatChipInputEvent, processValue: (component, value: string) => void) {
    const input = event.input;
    const value = event.value.trim();

    if (value) {
      processValue(this, value);
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addSkill(event: MatChipInputEvent): void {
    function processValue(component, value: string): void {
      component.game.skills.push({name: value});
    }

    this.processMatChipInputEvent(event, processValue);

    // update form control (necessary for correct validation)
    this._updateSkillAndAttributeFormControls();
  }

  removeSkill(skill: SkillDefinition): void {
    delete_item_from_array(skill, this.game.skills);
  }

  addAttribute(event: MatChipInputEvent): void {
    function process_value(component, value: string): void {
      console.log(component.skillForm);
      const attribute = {name: value, relatedSkills: [], bias: 0, aggregation: null, rounding: null};
      component.game.attributes.push(attribute);
      component._addFormControlsForSkillCombination(component.skillForm, attribute);
    }

    this.processMatChipInputEvent(event, process_value);

    // update form control (necessary for correct validation)
    this._updateSkillAndAttributeFormControls();
  }

  addSkillToAttribute(event: MatChipInputEvent, attribute: AttributeDefinition): void {
    function processValue(component, value: string): void {
      attribute.relatedSkills.push({name: value});
      component._resetFormControlsForSkillCombination(component.skillForm, attribute);
      component.game.skills.push({name: value});
    }

    this.processMatChipInputEvent(event, processValue);
    // update form control (necessary for correct validation)
    this._updateSkillAndAttributeFormControls();
  }

  private _updateSkillAndAttributeFormControls() {
    this.skillForm.controls.skill.setValue('');
    this.skillForm.controls.skills.setValue(this.game.skills);
    this.skillForm.controls.attribute.setValue('');
    this.skillForm.controls.attributes.setValue(this.game.attributes);
  }

  removeSkillFromAttribute(skill: SkillDefinition, attribute: AttributeDefinition): void {
    this.removeSkill(skill);
    delete_item_from_array(skill, attribute.relatedSkills);
  }

  private _addFormControlsForSkillCombination(form: FormGroup, skillCombination: SkillCombination): void {
    form.addControl(skillCombination.name + '_skill', new FormControl(''));
    form.addControl(skillCombination.name + '_skills', new FormControl(skillCombination.relatedSkills, Validators.required));
    form.addControl(skillCombination.name + '_aggregation', new FormControl(skillCombination.aggregation, Validators.required));
    form.addControl(skillCombination.name + '_bias', new FormControl(skillCombination.bias, Validators.required));
    form.addControl(skillCombination.name + '_rounding', new FormControl(skillCombination.rounding, Validators.required));
  }

  private _deleteFormControlsForSkillCombination(form: FormGroup, skillCombination: SkillCombination): void {
    form.removeControl(skillCombination.name + '_skill');
    form.removeControl(skillCombination.name + '_skills');
    form.removeControl(skillCombination.name + '_aggregation');
    form.removeControl(skillCombination.name + '_bias');
    form.removeControl(skillCombination.name + '_rounding');
  }

  private _resetFormControlsForSkillCombination(form: FormGroup, skillCombination: SkillCombination): void {
    // update form control (necessary for correct validation)
    form.controls[skillCombination.name + '_skill'].setValue(null);
    form.controls[skillCombination.name + '_skills'].setValue(skillCombination.relatedSkills);
  }

  removeAttribute(attribute: AttributeDefinition): void {
    const context = {message: 'This will delete the attribute "' + attribute.name + '" and all associated skills.'};
    this.dialogService.openConfirmDialog(context).subscribe(confirmed => {
      if (confirmed) {
        delete_item_from_array(attribute, this.game.attributes);
        this._deleteFormControlsForSkillCombination(this.skillForm, attribute);
      }
    });
  }

  addStressBar(event: MatChipInputEvent): void {
    function process_value(component, value: string): void {
      const stressBar = {name: value, relatedSkills: [], bias: 0, aggregation: null, rounding: null};
      component.game.stressBars.push(stressBar);
      component._addFormControlsForSkillCombination(component.stressForm, stressBar);
    }

    this.processMatChipInputEvent(event, process_value);
  }

  addSkillToStressBar(event: MatChipInputEvent, stressBar: StressBarDefinition): void {
    function processValue(component, value: string): void {
      stressBar.relatedSkills.push({name: value});
      component._resetFormControlsForSkillCombination(component.stressForm, stressBar);
    }

    this.processMatChipInputEvent(event, processValue);
  }

  selectSkillForStressBar(event: MatAutocompleteSelectedEvent, stressBar: StressBarDefinition): void {
    stressBar.relatedSkills.push({name: event.option.viewValue});
    this._resetFormControlsForSkillCombination(this.stressForm, stressBar);
  }

  removeSkillFromStressBar(skill: SkillDefinition, stressBar: StressBarDefinition): void {
    delete_item_from_array(skill, stressBar.relatedSkills);
  }

  removeStressBar(stressBar: StressBarDefinition): void {
    const context = {message: 'This will delete the stressBar "' + stressBar.name + '" and all associated skills.'};
    this.dialogService.openConfirmDialog(context).subscribe(confirmed => {
      if (confirmed) {
        delete_item_from_array(stressBar, this.game.stressBars);
        this._deleteFormControlsForSkillCombination(this.stressForm, stressBar);
      }
    });
  }

  onHasMaxNumberOfStuntsToggle() {
    this.game.hasMaxNumberOfStunts = !this.game.hasMaxNumberOfStunts;
    this.stuntForm.controls.hasMaxNumberOfStunts.setValue(false);
    this.stuntForm.controls.numberOfMaxStunts.setValue(this.game.numberOfMaxStunts);
  }

  onReset(stepper: MatVerticalStepper) {
    const context = {message: 'All previously entered data will be irreversibly lost.'};
    this.dialogService.openConfirmDialog(context).subscribe(confirmed => {
      if (confirmed) {
        for (const attribute of this.game.attributes) {
          this._deleteFormControlsForSkillCombination(this.skillForm, attribute);
        }
        for (const stressBar of this.game.stressBars) {
          this._deleteFormControlsForSkillCombination(this.stressForm, stressBar);
        }
        this.game = DefaultGame;
        stepper.reset();
      }
    });
  }

  filter<T extends Namable>(collection: T[], form: FormGroup, controlName: string, negate: boolean = false): T[] {
    return this.filters.fuzzyFilter(collection, form.controls[controlName].value, negate);
  }
}
