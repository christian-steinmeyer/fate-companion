import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';

import {ValidationService} from '../services/validation/validation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    public authenticator: AngularFireAuth,
    public formBuilder: FormBuilder,
    private router: Router,
    public validator: ValidationService,
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  doGoogleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    this.authenticator.auth
      .signInWithPopup(provider)
      .then(this.redirectHome).catch(error => {
      console.log(error);
    });
  }

  doEmailLogin(email, password) {
    const credential = firebase.auth.EmailAuthProvider.credential(email, password);
    this.authenticator.auth.signInWithCredential(credential)
      .then(this.redirectHome).catch(error => {
      console.log(error);
    });
  }

  redirectHome() {
    this.router.navigate(['home']).catch(error => {
      console.log(error);
    });
  }

  onSubmit(userInput) {
    this.doEmailLogin(userInput.email, userInput.password);
    this.loginForm.reset();
  }

}
