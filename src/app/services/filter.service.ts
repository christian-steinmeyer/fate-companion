import {Injectable} from '@angular/core';
import {Namable} from '../../lib/data.types/base';
import {SkillCombination} from '../../lib/data.types/skill';
import * as fuzzysort from 'fuzzysort';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor() { }

  filter(collection: Namable[] | SkillCombination[], value: string, negate: boolean = false): Namable[] | SkillCombination[] {
    const filterValue = value ? value.toLowerCase() : '';
    if (negate) {
      return collection.filter(item => item.name.toLowerCase().indexOf(filterValue) === -1);
    } else {
      return collection.filter(item => item.name.toLowerCase().indexOf(filterValue) > -1);
    }
  }

  fuzzyFilter<T extends Namable>(collection: T[], value: string, negate: boolean = false): T[] {
    const filterValue = value ? value.toLowerCase() : '';
    const matches = fuzzysort.go(filterValue, collection, {key: 'name'});
    const results = matches.map(match => match.obj);
    if (negate) {
      return collection.filter(item => !results.includes(item));
    } else {
      return results;
    }
  }
}
