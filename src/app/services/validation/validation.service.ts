import {Injectable} from '@angular/core';
import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() {
  }

  private static handleCustomErrors(
    prefix: string,
    validator: ValidatorFn,
    controls: string[],
    formGroup: FormGroup,
    hasNoErrors: boolean
  ) {
    const errors = {};
    errors[prefix + validator.name] = true;

    if (!controls) {
      controls = Object.keys(formGroup.controls);
    }
    if (!hasNoErrors) {
      for (const control of controls) {
        formGroup.controls[control].setErrors(errors);
      }
    }
    return hasNoErrors ? null : errors;
  }

  /**
   * Returns an error message for the most severe error present on a given field of a form group.
   *
   * @param formGroup Form containing the controls
   * @param field Field to be evaluated
   */
  validate(formGroup: FormGroup, field: string) {
    // convenience getter for easy access to the field's errors
    function hasError(flag: string) {
      return formGroup.controls[field].hasError(flag);
    }

    function getError(flag: string) {
      return formGroup.controls[field].getError(flag);
    }

    if (hasError('required')) {
      return 'Field is required';
    }
    if (hasError('atLeastOne_required')) {
      return 'This or another (possibly hidden) field is required';
    }
    if (hasError('email')) {
      return 'Not a valid email address';
    }
    if (hasError('min')) {
      const minimum = getError('min').min;
      return `Needs to be more than or equal to ${minimum}`;
    }
    if (hasError('minlength')) {
      const missing = getError('minlength').requiredLength - getError('minlength').actualLength;
      return `Needs at least ${missing} more to fulfill minimum length`;
    }
    if (hasError('noneOrAll_required')) {
      return 'Either this field is required or another one needs to be deselected';
    }
    if (hasError('matchConfirmation')) {
      return 'The two fields do not match';
    }
    return 'Unexpected Error in Validation Service';
  }

  matchConfirmation(original: string, confirmation: string) {
    const errors = {matchConfirmation: true};
    return (formGroup: FormGroup): ValidationErrors | null => {
      const originalControl = formGroup.controls[original];
      const confirmationControl = formGroup.controls[confirmation];

      // set error on confirmationControl if validation fails
      if (originalControl.value !== confirmationControl.value) {
        confirmationControl.setErrors(errors);
        return errors;
      }
      return null;
    };
  }

  noneOrAll = (validator: ValidatorFn, controls: string[] = null) => (formGroup: FormGroup): ValidationErrors | null => {
    const prefix = 'noneOrAll_';

    const none = !controls.some(k => !validator(formGroup.controls[k]));
    const all = controls.every(k => !validator(formGroup.controls[k]));
    const hasNoneOrAll = formGroup && formGroup.controls && (none || all);
    return ValidationService.handleCustomErrors(prefix, validator, controls, formGroup, hasNoneOrAll);
  }


  atLeastOne = (validator: ValidatorFn, controls: string[] = null) => (formGroup: FormGroup): ValidationErrors | null => {
    const prefix = 'atLeastOne_';
    const atLeastOne = controls.some(k => !validator(formGroup.controls[k]));
    const hasAtLeastOne = formGroup && formGroup.controls && atLeastOne;

    return ValidationService.handleCustomErrors(prefix, validator, controls, formGroup, hasAtLeastOne);
  }
}
