import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';

import {ValidationService} from '../services/validation/validation.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.sass']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(
    public authenticator: AngularFireAuth,
    public validator: ValidationService,
    private router: Router,
    public formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: this.validator.matchConfirmation('password', 'confirmPassword')
    });
  }

  doGoogleSignUp() {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    this.authenticator.auth
      .signInWithPopup(provider)
      .then(this.redirectHome).catch(error => {
      console.log(error);
    });
  }

  doEmailAccountCreation(email, password) {
    this.authenticator.auth.createUserWithEmailAndPassword(email, password)
      .then(this.redirectHome).catch(error => {
      console.log(error);
    });
  }

  redirectHome() {
    this.router.navigate(['home']).catch(error => {
      console.log(error);
    });
  }

  onSubmit(userInput) {
    this.doEmailAccountCreation(userInput.email, userInput.password);
    this.signUpForm.reset();
  }

}
