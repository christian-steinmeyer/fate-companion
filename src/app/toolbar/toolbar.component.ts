import {Component, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.sass']
})
export class ToolbarComponent implements OnInit {
  title = 'Fate Companion';

  constructor(public authenticator: AngularFireAuth, private router: Router) {
  }

  ngOnInit() {
  }

  logout() {
    this.authenticator.auth.signOut()
      .then(this.redirectLandingPage).catch(error => {
      console.log(error);
    });
  }

  redirectLandingPage() {
    this.router.navigate(['landing-page']).catch(error => {
      console.log(error);
    });
  }

}
