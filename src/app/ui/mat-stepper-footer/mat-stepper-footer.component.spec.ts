import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MatStepperFooterComponent} from './mat-stepper-footer.component';

describe('MatStepperFooterComponent', () => {
  let component: MatStepperFooterComponent;
  let fixture: ComponentFixture<MatStepperFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MatStepperFooterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatStepperFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
