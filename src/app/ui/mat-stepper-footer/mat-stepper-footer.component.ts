import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatVerticalStepper} from '@angular/material';

@Component({
  selector: 'app-mat-stepper-footer',
  templateUrl: './mat-stepper-footer.component.html',
  styleUrls: ['./mat-stepper-footer.component.sass']
})
export class MatStepperFooterComponent implements OnInit {
  @Input() first: boolean;
  @Input() last: boolean;
  @Input() stepper: MatVerticalStepper;
  @Output() notify = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
