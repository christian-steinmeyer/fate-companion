import {animate, keyframes, query, stagger, state, style, transition, trigger} from '@angular/animations';

export const speedDialFabAnimations = [
  trigger('fabToggler', [
    state('inactive', style({
      transform: 'rotate(0deg)'
    })),
    state('active', style({
      transform: 'rotate(225deg)'
    })),
    transition('* <=> *', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
  ]),
  trigger('fabBackgroundBlurrer', [
    state('inactive', style({
      opacity: 0
    })),
    state('active', style({
      opacity: 1
    })),
    transition('* <=> *', animate('250ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
  ]),
  trigger('speedDialStagger', [
    transition('* => *', [

      query(':enter', style({opacity: 0}), {optional: true}),

      query(':enter', stagger('50ms',
        [
          animate('250ms cubic-bezier(0.4, 0.0, 0.2, 1)',
            keyframes(
              [
                style({opacity: 0, transform: 'translateY(10px)'}),
                style({opacity: 1, transform: 'translateY(0)'}),
              ]
            )
          )
        ]
      ), {optional: true}),

      query(':leave', stagger('50ms',
        [
          animate('250ms cubic-bezier(0.4, 0.0, 0.2, 1)',
            keyframes(
              [
                style({opacity: 1, transform: 'translateY(0px)'}),
                style({opacity: 0, transform: 'translateY(10px)'}),
              ]
            )
          )
        ]
      ), {optional: true})

    ])
  ])
];
