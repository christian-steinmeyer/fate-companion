import {Component} from '@angular/core';
import {speedDialFabAnimations} from './speed-dial-fab.animations';

@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.sass'],
  animations: speedDialFabAnimations
})
export class SpeedDialFabComponent {
  fabOptions = [
    {
      icon: 'baby',
      target: '/character/new',
      title: 'New Character'
    },
    {
      icon: 'planet',
      target: '/game/new',
      title: 'New Game',
    }
  ];
  buttons = [];
  fabTogglerState = 'inactive';

  constructor() {
  }


  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabOptions;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }

}
