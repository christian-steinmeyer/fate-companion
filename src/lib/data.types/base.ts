export interface Namable {
  name: string;
}

export interface Valuable {
  value: number;
}

export interface Rollable {
  roll(): number;
}

export enum UseStatus {
  Free = 'Free',
  Used = 'Used'
}

export interface Usable {
  status: UseStatus;

  use(): void;
}

export interface Calculatable {
  formula: string;

  calculate(): number;
}

export interface CalculationDefinition {
  aggregation: string;
  bias: number;
  rounding: string;
}
