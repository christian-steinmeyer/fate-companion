import {Namable} from './base';
import {AttributeDefinition, DefaultSkillPyramid, SkillDefinition, SkillLevel} from './skill';
import {Consequence, StressBarDefinition} from './stress';
import {MoneySystem} from './money';
import {DefaultRuleSet, RuleSet} from './rules';


export interface GameDefinition extends Namable {
  // General Settings
  description: string;
  invitations: string[];
  ruleSet: RuleSet;

  // Skills and Attributes
  useAttributes: boolean;
  skills: SkillDefinition[];
  attributes: AttributeDefinition[];
  skillPyramid: Map<SkillLevel, number>;

  // Refresh, Stress and Consequences
  refreshRate: number;
  stressBars: StressBarDefinition[];
  consequences: Consequence[];

  // Stunts
  numberOfFreeStunts: number;
  hasMaxNumberOfStunts: boolean;
  numberOfMaxStunts: number;
  costPerStunt: number;

  // Extras
  useInventory: boolean;
  useMoney: boolean;
  moneySystem: MoneySystem;
  useExtras: boolean;
}


export const DefaultGame: GameDefinition = {
  name: '',
  description: '',
  invitations: [],
  ruleSet: DefaultRuleSet,

  useAttributes: false,
  skills: [],
  attributes: [],
  skillPyramid: DefaultSkillPyramid,

  refreshRate: 0,
  stressBars: [],
  consequences: [],

  numberOfFreeStunts: 0,
  hasMaxNumberOfStunts: false,
  numberOfMaxStunts: 0,
  costPerStunt: 0,

  useExtras: false,
  useInventory: false,
  useMoney: false,
  moneySystem: null
};
