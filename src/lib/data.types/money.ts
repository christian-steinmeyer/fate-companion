import {Namable, Valuable} from './base';

export interface Unit extends Namable, Valuable {
}

export interface MoneySystem {
  units: Unit[];

  convert(from: Unit, to: Unit, value: number): number;
}
