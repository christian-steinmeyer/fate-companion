import {Namable} from './base';

export interface RuleSet extends Namable {
  enforceSkillPyramid: boolean;
}

export const DefaultRuleSet: RuleSet = {
  name: 'Default',
  enforceSkillPyramid: true
};

export const RuleSets: RuleSet[] = [
  DefaultRuleSet
];
