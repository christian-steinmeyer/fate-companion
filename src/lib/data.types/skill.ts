import {Calculatable, CalculationDefinition, Namable, Rollable, Valuable} from './base';

export type SkillDefinition = Namable;

export interface SkillCombination extends Namable, CalculationDefinition {
  relatedSkills: SkillDefinition[];
}

export type AttributeDefinition = SkillCombination;

export interface SkillLevel extends Namable, Valuable {
}

const Average: SkillLevel = {
  name: 'Average',
  value: 1
};

const Fair: SkillLevel = {
  name: 'Fair',
  value: 2
};

const Good: SkillLevel = {
  name: 'Good',
  value: 3
};

const Great: SkillLevel = {
  name: 'Great',
  value: 4
};

const Superb: SkillLevel = {
  name: 'Superb',
  value: 5
};

export const DefaultSkillPyramid: Map<SkillLevel, number> =
  new Map<SkillLevel, number>([[Average, 4], [Fair, 3], [Good, 2], [Great, 1], [Superb, 0]]);


export interface Skill extends Namable, Rollable {
  level: number;
}

export interface Attribute extends Skill, Calculatable {
  relatedSkills: Skill[];
}

export interface SkillPyramid {
  skillLevels: Map<number, number>;
}
