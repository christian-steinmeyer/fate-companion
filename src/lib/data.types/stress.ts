import {Calculatable, Namable, Usable, Valuable} from './base';
import {Skill, SkillCombination} from './skill';

export type StressBarDefinition = SkillCombination;


export interface StressPoint extends Valuable, Usable {
  minimalRequirement: number;
}

export interface StressBar extends Namable, Calculatable {
  relatedSkills: Skill[];
  points: StressPoint[];
}

export interface Consequence extends Namable, Valuable, Usable {
}
