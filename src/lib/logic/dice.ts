import {Rollable} from '../data.types/base';

export enum FateDiceOutcome {
  Minus = -1,
  Neutral = 0,
  Plus = 1
}

function randomEnumValue(enumeration) {
  const options = Object.keys(enumeration);
  const randomKey = options[Math.floor(Math.random() * options.length)];
  return enumeration[randomKey];
}

class FateDice implements Rollable {
  roll() {
    return randomEnumValue(FateDiceOutcome);
  }
}

export class FateDiceThrow implements Rollable {
  bonus: number;
  dice: FateDice[];
  outcomes: FateDiceOutcome[];

  constructor(numberOfDice: number = 4, bonus: number = 0) {
    for (let i = 0; i < numberOfDice; i++) {
      this.dice.push(new FateDice());
    }
    this.bonus = bonus;
  }

  roll(): number {
    let result = this.bonus;
    for (const dice of this.dice) {
      const singleOutcome = dice.roll();
      this.outcomes.push(singleOutcome);
      result += singleOutcome;
    }
    return result;
  }
}
