import {Namable} from '../data.types/base';

export interface Aggregation extends Namable {
  aggregate(...values: number[]): number;
}

export interface Rounding extends Namable {
  round(value: number): number;
}

function sum(...values: number[]): number {
  return values.reduce((previousSum, current) => previousSum + current, 0);
}

function average(...values: number[]): number {
  return sum(...values) / values.length;
}

export const DefaultAggregations = [
  {name: 'Sum', aggregate: sum},
  {name: 'Average', aggregate: average}
];

export const DefaultRoundings = [
  {name: 'Nearest', round: Math.round},
  {name: 'Down', round: Math.floor},
  {name: 'Up', round: Math.ceil}
];
