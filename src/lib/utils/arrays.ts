export function delete_item_from_array(item: any, array: Array<any>): void {
  const index = array.indexOf(item, 0);
  if (index > -1) {
    array.splice(index, 1);
  }
}
